package com.duzq.ws;

import java.io.Serializable;

/**
 * 入参请求示例
 * @author duzq
 * @date 2021-03-20 19:32:01
 */
public class WebServiceRequest implements Serializable {
    /** * 页码 */
    private Integer pageIndex;
    /** * 页数大小 */
    private Integer pageSize;
    /** * 查询条件 */
    private OrderInfo orderInfo;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }
}
