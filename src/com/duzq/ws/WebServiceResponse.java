package com.duzq.ws;

import java.io.Serializable;

/**
 * 出参请求示例
 * @author duzq
 * @date 2021-03-20 19:32:01
 */
public class WebServiceResponse implements Serializable {

    /** * 状态码：200 成功 */
    private Integer code;
    /** * 提示消息 */
    private String msg;

    /** * 订单信息 */
    private OrderInfo orderInfo;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }
}
